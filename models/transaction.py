# Models
class Transaction():

    def __init__(self, db):
        id = db.Column(db.Integer, primary_key=True)
        description = db.Column(db.String(50), unique=False, nullable=False)
        amount = db.Column(db.Integer, nullable=False)

    # repr method represents how one object of this datatable
    # will look like
    def __repr__(self):
        return f"ID : {self.id}, Desc: {self.description}, Amount: {self.amount}"